import React from "react";
import "./Buttons.css";

const Buttons = props => {
  return (
    <button className="inner" onClick={() => props.onClick(props.name)}>
      {props.name}
    </button>
  );
};

export default Buttons;
