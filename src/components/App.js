import React from "react";
import Buttons from "./Buttons";

import "./App.css";

class App extends React.Component {
  state = {
    names: [],
    buttons: ["MAIL", "PHONE", "CITY"],
    city: false,
    phone: false,
    mail: false
  };
  // componentDidMount() {
  //   this.getData();
  // }

  /*connection to API */
  getData = () => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "https://randomuser.me/api/?results=1", true);
    xhr.send();
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("connected to database");
        const data = JSON.parse(xhr.response);
        const user = data.results;
        this.setState(prevState => ({
          names: prevState.names.concat(user)
        }));
        console.log(this.state.names);
      }
    };
  };

  handleClick = id => {
    if (id === "CITY") {
      this.setState({
        city: true,
        phone: false,
        mail: false
      });
    } else if (id === "PHONE") {
      this.setState({
        city: false,
        phone: true,
        mail: false
      });
    } else {
      this.setState({
        city: false,
        phone: false,
        mail: true
      });
    }
  };
  render() {
    const users = this.state.names.map(user => (
      <div className="users" key={user.login.uuid}>
        {" "}
        <div className="buttons">
          {" "}
          {this.state.buttons.map(el => (
            <Buttons onClick={this.handleClick} key={el} name={el} />
          ))}
        </div>{" "}
        <h2> {user.name.first} </h2> <h2>{user.name.last}</h2>{" "}
        <img src={user.picture.large} alt={user.name.last} />
        <div style={{ height: "50px" }}>
          <p>
            {this.state.phone ? user.phone : null}
            {this.state.city ? user.location.city : null}
            {this.state.mail ? user.email : null}
          </p>{" "}
        </div>
      </div>
    ));

    return (
      <div>
        <button className="dataFetch" onClick={this.getData}>
          {" "}
          pobierz dane{" "}
        </button>

        {this.state.names.length > 0 ? users : null}
      </div>
    );
  }
}

export default App;
